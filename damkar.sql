-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 25, 2023 at 06:24 PM
-- Server version: 8.0.31
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `damkar`
--

-- --------------------------------------------------------

--
-- Table structure for table `absen`
--

CREATE TABLE `absen` (
  `id` int NOT NULL,
  `id_anggota` int NOT NULL,
  `status_piket` int NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `create_by` varchar(100) NOT NULL,
  `create_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `absen`
--

INSERT INTO `absen` (`id`, `id_anggota`, `status_piket`, `keterangan`, `create_by`, `create_date`) VALUES
(5, 1, 1, 'ya', '1', '2023-07-25'),
(6, 2, 2, 'tidak', '', '2023-07-25'),
(7, 4, 3, 'tidak', '', '2023-07-25'),
(8, 5, 3, 'tidak', '', '2023-07-25'),
(9, 6, 1, 'ya', '', '2023-07-25'),
(10, 7, 2, 'tidak', '', '2023-07-25'),
(11, 8, 2, 'tidak', '', '2023-07-25'),
(26, 1, 3, '', '2023-07-25', '0000-00-00'),
(27, 2, 1, '', '2023-07-25', '0000-00-00'),
(28, 4, 2, '', '2023-07-25', '0000-00-00'),
(29, 5, 2, '', '2023-07-25', '0000-00-00'),
(30, 6, 3, '', '2023-07-25', '0000-00-00'),
(31, 7, 1, '', '2023-07-25', '0000-00-00'),
(32, 8, 1, '', '2023-07-25', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id` int NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `group_piket` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id`, `nama`, `jabatan`, `group_piket`) VALUES
(1, 'yuda', 'Anggota', 'A'),
(2, 'pratama', 'Anggota', 'B'),
(4, 'Joni', 'Anggota', 'C'),
(5, 'Jono', 'Anggota', 'C'),
(6, 'Dian', 'Anggota', 'A'),
(7, 'Ahmad', 'Anggota', 'B'),
(8, 'Saepul', 'Anggota', 'B');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_piket`
--

CREATE TABLE `jadwal_piket` (
  `id` int NOT NULL,
  `tanggal` date NOT NULL,
  `piket` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `jadwal_piket`
--

INSERT INTO `jadwal_piket` (`id`, `tanggal`, `piket`) VALUES
(1, '2023-07-25', 'A'),
(2, '2023-07-27', 'B'),
(3, '2023-07-28', 'C'),
(4, '2023-07-29', 'A'),
(5, '2023-07-30', 'B');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `sertivicate` varchar(30) NOT NULL,
  `bio` varchar(100) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int NOT NULL,
  `is_active` int NOT NULL,
  `date_created` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `jenis_kelamin`, `sertivicate`, `bio`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(11, 'admin', 'admin@gmail.com', '', '', '', 'logo.png', '$2y$10$di8Fqt619ru5hrG6OZI4ouqOJRC8H02LkF.6V0q./mtc9srt0OuRu', 1, 1, 1656226646),
(25, 'Imam', 'pemimpin.kelompok', '', '', '', 'logo.png', '$2y$10$oRoPLDuaLd.AIF4fkZt5JOLXYP9xGy8OWdx6zN86trLRgenSPOofa', 1, 1, 1690260389),
(26, 'Maulana', 'pemimpin.apel', '', '', '', 'logo.png', '$2y$10$U1w3ABTL/VlUPgQ1cdjR8ezYRyXRDitAzyUoV8xJpSgiLFEON8WdK', 2, 1, 1690263773);

-- --------------------------------------------------------

--
-- Table structure for table `user_accsess_menu`
--

CREATE TABLE `user_accsess_menu` (
  `id` int NOT NULL,
  `role_id` int NOT NULL,
  `menu_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_accsess_menu`
--

INSERT INTO `user_accsess_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'member');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int NOT NULL,
  `menu_id` int NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Dashboard', 'Admin', 'fas fa-fw fa-tachometer-alt', 1),
(2, 2, 'Mahasiswa', 'User', 'fas fa-fw fa-user-graduate', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absen`
--
ALTER TABLE `absen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_piket`
--
ALTER TABLE `jadwal_piket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_accsess_menu`
--
ALTER TABLE `user_accsess_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absen`
--
ALTER TABLE `absen`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `jadwal_piket`
--
ALTER TABLE `jadwal_piket`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_accsess_menu`
--
ALTER TABLE `user_accsess_menu`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
